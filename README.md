## Добавление раннера

1. Добавляем хелм репозиторий гитлаба

```bash
helm repo add gitlab https://charts.gitlab.io
```

2. Ставим раннер

```bash
kubectl create namespace gitlab
helm install --namespace gitlab gitlab-runner gitlab/gitlab-runner \
  --set rbac.create=true \
  --set runners.privileged=true \
  --set gitlabUrl=https://gitlab.com/ \
  --set runnerRegistrationToken=<token from project settings/CI/CD/Runners>
```
3. Выключаем шаренные раннеры в Settings/CI/CD/Runners

## Подготовка Kubernetes к деплою

1. Создаем неймспейс для приложения 

```bash
kubectl create ns my-kubernetes-project
```

2. Создаем RBAC объекты для доступа раннера к API Kubernetes

```bash
kubectl create sa deploy -n my-kubernetes-project
kubectl create rolebinding deploy \
  -n my-kubernetes-project \
  --clusterrole edit \
  --serviceaccount my-kubernetes-project:deploy
```

3. Получам токен от созданного сервисаккаунта

```bash
kubectl get secret -n my-kubernetes-project \
  $(kubectl get sa -n my-kubernetes-project deploy \
    -o jsonpath='{.secrets[].name}') \
  -o jsonpath='{.data.token}'
```

4. Переходим в интерфейс гитлаба. В левом меню находим Settings, далее CI/CD
и далее Variables и нажимаем Expand
В левое поле вводим имя переменной
K8S_CI_TOKEN
В правое поле вводим скопированный токен
Protected выключаем!
Masked включаем!
5. Далее в том же левом меню в Settings > Repository находим Deploy tokens и нажимаем Expand.
В поле Name вводим
k8s-pull-token
И ставим галочку рядом с read_registry.
Все остальные поля оставляем пустыми.
Нажимаем Create deploy token.
НЕ ЗАКРЫВАЕМ ОКНО БРАУЗЕРА!
6. Возвращаемся в консоль.
Создаем image pull secret для того чтобы кубернетис мог пулить имаджи из гитлаба

```bash
kubectl create secret docker-registry my-kubernetes-project-image-pull \
  --docker-server registry.gitlab.com \
  --docker-email 'admin@mycompany.com' \
  --docker-username '<первая строчка из окна создания токена в gitlab>' \
  --docker-password '<вторая строчка из окна создания токена в gitlab>' \
  --namespace my-kubernetes-project
```

Соответсвенно подставляя на место <> нужные параметры.

7. Создаем секрет с паролем к БД

```bash
kubectl create secret generic my-kubernetes-project \
  -n my-kubernetes-project \
  --from-literal db-password=<сохраненный ранее пароль от БД>
```
8. Ставим ingress-nginx(Опционально)
helm upgrade --install ingress-nginx ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace

## Подготовка манифестов Kubernetes для проекта

1. Копируем директорию .kube из текущего репозитория в репозиторий проекта на гитлабе
1. Проставляем в файле deployment.yaml параметры подключения к БД
1. Пушим изменения, ждем окончания деплоя
1. По IP адресу балансировщика созданного облаком проверяем работу приложения.
Его можно получить с помощью команды

```bash
kubectl get svc -n ingress-nginx nginx-ingress-controller -o jsonpath='{.status.loadBalancer.ingress[].ip}'
```

Выполняем пару запросов для проверки

```bash
curl -k https://<INGRESS IP>/users -X POST -H "Content-Type: application/json" --data '{"name":"Ivan Ivanov","location":"Russia/Perm","age":31}'
curl -k https://<INGRESS IP>/users
```






